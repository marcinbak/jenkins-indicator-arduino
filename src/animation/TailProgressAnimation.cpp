/*
 * TailProgressAnimation.cpp
 *
 *  Created on: May 22, 2016
 *      Author: daber
 */

#include "TailProgressAnimation.h"
#include "color_utils.h"

TailProgressAnimation::TailProgressAnimation(Adafruit_NeoPixel* aNeo,
		uint8_t segments) :
		BaseAnimation(aNeo, segments) {
	r = (uint8_t*) malloc(segments);
	g = (uint8_t*) malloc(segments);
	b = (uint8_t*) malloc(segments);
	memset(r, 0, segments);
	memset(g, 0, segments);
	memset(b, 0, segments);

}

TailProgressAnimation::~TailProgressAnimation() {
	free(r);
	free(g);
	free(b);
	r = nullptr;
	g = nullptr;
	b = nullptr;
}

void TailProgressAnimation::draw() {
	for (int i = 0; i < segments; i++) {
		int seg = rotate(i - offset);
		uint32_t color = Adafruit_NeoPixel::Color(r[seg], g[seg], b[seg]);
		neo->setPixelColor(i, color);
	}
	neo->show();
}

void TailProgressAnimation::onTimePassed(uint32_t delta) {
	BaseAnimation::onTimePassed(delta);

	if (isRunning) {
		uint32_t segment_duration = duration / segments;
		offset = currentDuration / segment_duration;
		offset = offset % segments;
	}
}

void TailProgressAnimation::calcuatePattern() {

	uint16_t r_dropped = red(color);
	uint16_t g_dropped = green(color);
	uint16_t b_dropped = blue(color);
	uint16_t progress_pixel = progress*segments/255;
	for (int i = (int) segments - 1; i >= 0; i--) {
		if (i >= segments - (int) progress_pixel) {
			r[i] = r_dropped;
			g[i] = g_dropped;
			b[i] = b_dropped;
			r_dropped = r_dropped * drop / 255;
			g_dropped = g_dropped * drop / 255;
			b_dropped = b_dropped * drop / 255;
		} else {
			r[i] = 0;
			g[i] = 0;
			b[i] = 0;
		}
	}
}

void TailProgressAnimation::setProgress(uint8_t aProgress) {
	progress = aProgress;
	calcuatePattern();
}

void TailProgressAnimation::setColor(uint32_t aColor) {
	BaseAnimation::setColor(aColor);
	calcuatePattern();
}

void TailProgressAnimation::reset() {
	BaseAnimation::reset();
	offset = 0;
}

void TailProgressAnimation::setFill(uint8_t fill) {
	setProgress(fill);
}

