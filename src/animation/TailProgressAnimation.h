/*
 * TailProgressAnimation.h
 *
 *  Created on: May 22, 2016
 *      Author: daber
 */

#ifndef TAILPROGRESSANIMATION_H_
#define TAILPROGRESSANIMATION_H_

#include "IProgressAnimation.h"
#include "BaseAnimation.h"
#include <Adafruit_NeoPixel.h>
#include "rotation_utils.h"
class TailProgressAnimation: public BaseAnimation, public IProgressAnimation {

protected:
	uint8_t* r;
	uint8_t* g;
	uint8_t* b;

	uint32_t progress =0;

	uint8_t drop =200;
	uint8_t offset = 0;

	inline int rotate(int p) {
		return rotate_len(p,segments);
	}

	void calcuatePattern();

public:
	TailProgressAnimation(Adafruit_NeoPixel* neo,uint8_t segments);
	virtual ~TailProgressAnimation();
	virtual void draw();
	virtual void onTimePassed(uint32_t delta);
	virtual void setProgress(uint8_t progress);
	virtual void setFill(uint8_t fill /*0-255*/);
	virtual void setColor(uint32_t color);
	virtual void reset();


};

#endif /* TAILPROGRESSANIMATION_H_ */
