/*
 * color_utils.h
 *
 *  Created on: May 22, 2016
 *      Author: daber
 */

#ifndef COLOR_UTILS_H_
#define COLOR_UTILS_H_

static const uint8_t mask =  0xFF;

inline uint8_t red(uint32_t color){
	return (color >> 16)&mask;
}

inline uint8_t green(uint32_t color){
	return (color >> 8)&mask;
}

inline uint8_t blue(uint32_t color){
	return color&mask;
}




#endif /* COLOR_UTILS_H_ */
