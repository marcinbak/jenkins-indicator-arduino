/*
 * Indicator.h
 *
 *  Created on: Jun 11, 2016
 *      Author: mdabrowski
 */

#ifndef INDICATOR_H_
#define INDICATOR_H_
#include <Adafruit_NeoPixel.h>
#include <RF24.h>
#include "animation/Animator.h"
#include "animation/TailProgressAnimation.h"
#include "animation/TailProgressOverSamplingAnimation.h"
#include "animation/IProgressAnimation.h"
#include "animation/BrightnessAnimation.h"

class Indicator {
public:
	enum State {
		UNKNOWN = 0,
		SUCCCESS = 1,
		FAIL = 2,
		BUILDING_SUCCESS = 3,
		BUILDING_FAIL = 4
	};
	Indicator(Adafruit_NeoPixel& aPixel, RF24& aNrf,uint8_t aDeviceCode);
	virtual ~Indicator();
	void setup();
	void loop();

protected:
	uint8_t deviceCode;
	State currentState;
	Adafruit_NeoPixel& pixel;
	RF24& rf24;
	BrightnessAnimation successAnim;
	BrightnessAnimation failAnim;
	TailProgressOverSamplingAnimation successProgressAnim;
	TailProgressOverSamplingAnimation failProgressAnim;
	Animator animator;
};

#endif /* INDICATOR_H_ */
