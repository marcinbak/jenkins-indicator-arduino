/*
 * IAnimator.h
 *
 *  Created on: May 22, 2016
 *      Author: daber
 */

#ifndef ANIMATOR_H_
#define ANIMATOR_H_

#include "IAnimation.h"
#include <stdint.h>
#include <Adafruit_NeoPixel.h>
class Animator {

Adafruit_NeoPixel* neo;
private:
	bool isFirstTime = true;
	uint32_t currentTime = 0;
	IAnimation* animation = nullptr;
public:
	Animator(Adafruit_NeoPixel* pixel){
		neo=pixel;
	}
	void notifyTimePass();
	void setAnimation(IAnimation* animation);
};

#endif /* ANIMATOR_H_ */
