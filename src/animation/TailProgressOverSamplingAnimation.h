/*
 * TailProgressOverSamplingAnimation.h
 *
 *  Created on: Jul 28, 2016
 *      Author: mdabrowski
 */

#ifndef ANIMATION_TAILPROGRESSOVERSAMPLINGANIMATION_H_
#define ANIMATION_TAILPROGRESSOVERSAMPLINGANIMATION_H_

#include "TailProgressAnimation.h"

class TailProgressOverSamplingAnimation: public TailProgressAnimation {
private:
	uint8_t oversampleRatio;
public:
	TailProgressOverSamplingAnimation(Adafruit_NeoPixel* neo,uint8_t segments, uint8_t oversampleRatio);
	virtual ~TailProgressOverSamplingAnimation();
	virtual void draw();
};

#endif /* ANIMATION_TAILPROGRESSOVERSAMPLINGANIMATION_H_ */

