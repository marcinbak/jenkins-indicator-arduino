Arduino Nano pinout connections

Neopixel
		DATA_IN ->  D3

NRF24 	CNS -> D8
		CE 	-> D7
		MISO -> MISO (D12)
		MOSI -> MOSI (D11)
		SCK  -> SCK  (D13)
		IRQ  -> D2