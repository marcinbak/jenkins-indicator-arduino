/*
 * BrightnessAnimation.h
 *
 *  Created on: May 22, 2016
 *      Author: daber
 */

#ifndef BRIGHTNESSANIMATION_H_
#define BRIGHTNESSANIMATION_H_

#include "BaseAnimation.h"

class BrightnessAnimation: public BaseAnimation {
public:
	BrightnessAnimation(Adafruit_NeoPixel* neo, uint8_t segments);
	virtual ~BrightnessAnimation();
	virtual void draw();
};

#endif /* BRIGHTNESSANIMATION_H_ */
