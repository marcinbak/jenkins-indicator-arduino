/*
 * IProgressAnimation.h
 *
 *  Created on: May 22, 2016
 *      Author: daber
 */

#ifndef IPROGRESSANIMATION_H_
#define IPROGRESSANIMATION_H_
#include "IAnimation.h"
#include <stdint.h>

class IProgressAnimation: public virtual IAnimation {
protected:
	uint8_t progress = 0;

public:
	virtual ~IProgressAnimation() {};
	virtual void setProgress(uint8_t progress) = 0;
};

#endif /* IPROGRESSANIMATION_H_ */
