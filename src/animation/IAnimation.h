/*
 * IAnimation.h
 *
 *  Created on: May 22, 2016
 *      Author: daber
 */

#ifndef IANIMATION_H_
#define IANIMATION_H_
#include <stdint.h>

class IAnimation{
protected:

public:
	virtual ~IAnimation(){};
	virtual void draw() = 0;
	virtual void onTimePassed(uint32_t delta) = 0;
	virtual void reset() = 0;

};


#endif /* IANIMATION_H_ */
