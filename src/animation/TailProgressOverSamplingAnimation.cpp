/*
 * TailProgressOverSamplingAnimation.cpp
 *
 *  Created on: Jul 28, 2016
 *      Author: mdabrowski
 */

#include "TailProgressOverSamplingAnimation.h"
#include "color_utils.h"

TailProgressOverSamplingAnimation::TailProgressOverSamplingAnimation(
		Adafruit_NeoPixel* neo, uint8_t segments, uint8_t aOversampleRatio) :
		oversampleRatio(aOversampleRatio), TailProgressAnimation(neo,
				segments * oversampleRatio) {
	drop = 245;
}

TailProgressOverSamplingAnimation::~TailProgressOverSamplingAnimation() {
}


void TailProgressOverSamplingAnimation::draw() {
	for (int i = 0; i < segments; i = i + oversampleRatio) {
		uint16_t red = 0, green = 0, blue = 0;
		for (int j = 0; j < oversampleRatio; j++) {
			int seg = rotate(i + j - offset -oversampleRatio/2);
			red +=r[seg];
			green += g[seg];
			blue += b[seg];
		}
		uint32_t color = Adafruit_NeoPixel::Color(red/oversampleRatio,green/oversampleRatio,blue/oversampleRatio);
		neo->setPixelColor(rotate_len(-i/oversampleRatio,neo->numPixels()), color);
	}
	neo->show();
}
