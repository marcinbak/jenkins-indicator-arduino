/*
 * rotation_utils.h
 *
 *  Created on: Jul 28, 2016
 *      Author: mdabrowski
 */

#ifndef ANIMATION_ROTATION_UTILS_H_
#define ANIMATION_ROTATION_UTILS_H_


inline int rotate_len(int i, int length) {
		i = i % length;
		while (i < 0) {
			i = length + i;
		}
		return i;
	}

#endif /* ANIMATION_ROTATION_UTILS_H_ */
