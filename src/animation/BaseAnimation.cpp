/*
 * BaseAnimation.cpp
 *
 *  Created on: May 22, 2016
 *      Author: daber
 */

#include "BaseAnimation.h"

BaseAnimation::BaseAnimation(Adafruit_NeoPixel* pixel,uint8_t numSegments) {
	neo = pixel;
	segments = numSegments;
}


BaseAnimation::~BaseAnimation() {
	neo = nullptr;
}
void BaseAnimation::onTimePassed(uint32_t delta) {

		if (!isRunning) {
			isRunning = true;
			currentDuration = 0;
		} else {
			currentDuration += delta;
			currentDuration = currentDuration % duration;
		}
}


