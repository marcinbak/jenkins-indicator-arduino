// Do not remove the include below
#include <Adafruit_NeoPixel.h>
#include <RF24.h>
#include "jenkins_indicator.h"
#include "animation/TailProgressAnimation.h"
#include "animation/BrightnessAnimation.h"
#include "animation/Animator.h"
#include "Indicator.h"
#include <EEPROM.h>

#define NEOPIXEL_DATA_PIN 3

#define DEVICE_CODE_EPROM_INDEX  0
#define PIXEL_NUM_EPROM_INDEX  1

Adafruit_NeoPixel* pixel;

RF24* radio;
Indicator* indicator;
//The setup function is called once at startup of the sketch
void setup() {

	pinMode(2,INPUT);
	digitalWrite(2,LOW);
	Serial.begin(9600);
	Serial.println("begin");
	uint8_t pixelsNum;
	uint8_t deviceCode;
	EEPROM.get(DEVICE_CODE_EPROM_INDEX, deviceCode);
	EEPROM.get(PIXEL_NUM_EPROM_INDEX, pixelsNum);
	pixel = new Adafruit_NeoPixel(pixelsNum, NEOPIXEL_DATA_PIN,
			NEO_GRB | NEO_KHZ800);
	radio = new RF24(7, 8);
	indicator = new Indicator(*pixel, *radio, deviceCode);
	indicator->setup();
}

// The loop function is called in an endless loop
void loop() {
	indicator->loop();
}
