/*
 * BaseAnimation.h
 *
 *  Created on: May 22, 2016
 *      Author: daber
 */

#ifndef BASEANIMATION_H_
#define BASEANIMATION_H_

#include "IAnimation.h"
#include <Adafruit_NeoPixel.h>
class BaseAnimation: public virtual IAnimation {
protected:
	Adafruit_NeoPixel* neo;

	uint32_t color = Adafruit_NeoPixel::Color(255,255,255);
	uint8_t segments = 16;
	uint32_t duration = 3000;
	uint32_t currentDuration = 0;
	bool isRunning = false;


public:
	BaseAnimation(Adafruit_NeoPixel* pixel,uint8_t numSegments);
	virtual void onTimePassed(uint32_t delta);
	virtual ~BaseAnimation();
	virtual uint32_t getDuration() const {
		return duration;
	}

	virtual void setDuration(uint32_t duration) {
		this->duration = duration;
	}

	void setColor(uint32_t aColor) {
		color = aColor;
	}
	virtual void reset(){
		currentDuration = 0;
		isRunning = false;
	}

};

#endif /* BASEANIMATION_H_ */
