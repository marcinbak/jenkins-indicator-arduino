/*
 * Animator.cpp
 *
 *  Created on: May 22, 2016
 *      Author: daber
 */

#include "Animator.h"

void Animator::notifyTimePass() {
	uint32_t m = millis();
	if (isFirstTime) {
		currentTime = m;
		isFirstTime = false;
	} else {
		uint32_t delta = m - currentTime;
		currentTime = m;
		if (animation != nullptr) {
			animation->onTimePassed(delta);
			animation->draw();
		}
	}

}

void Animator::setAnimation(IAnimation* anAnimation) {
	if(animation == anAnimation){
		return;
	}
	animation = anAnimation;
	isFirstTime = true;
	neo->clear();
	neo->show();
	if(animation != nullptr){
		animation->reset();
	}
}
