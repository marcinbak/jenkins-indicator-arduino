/*
 * BrightnessAnimation.cpp
 *
 *  Created on: May 22, 2016
 *      Author: daber
 */

#include "BrightnessAnimation.h"
#include <Adafruit_NeoPixel.h>
#include "color_utils.h"

BrightnessAnimation::BrightnessAnimation(Adafruit_NeoPixel* neo, uint8_t segments) :BaseAnimation(neo,segments) {
}




BrightnessAnimation::~BrightnessAnimation() {
	// TODO Auto-generated destructor stub
}

void BrightnessAnimation::draw() {
	uint8_t r = red(color);
	uint8_t g = green(color);
	uint8_t b = blue(color);
	uint32_t half = duration/2;
	uint16_t dump;
	if(currentDuration <= half){
		dump = currentDuration*255/half;

	}else{
		dump = 255 -(currentDuration-half)*255/half;

	}

	r =r *dump /255;
	g =g *dump /255;
	b =b *dump /255;
	uint32_t c = Adafruit_NeoPixel::Color(r,g,b);
	for(int i =0;i<segments;i++){
		neo->setPixelColor(i,c);
	}
	neo->show();

}
