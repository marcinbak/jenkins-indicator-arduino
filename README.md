# README #

This repository is a part of CI Indicators project, described in detail on Neofonie Tech Blog.

### What is this repository for? ###

* Arduino part of the project CI Indicators
* Version 1.0

### How do I get set up? ###

To configure the Arduino board, please perform the following steps manually:

1. Checkout the project git repository from Bitbucket https://bitbucket.org/neofoniemobile/jenkins-indicator-arduino.git master

2. Install platform.io CLI (see: http://platformio.org/get-started/cli)

3. Connect the arduino nano into usb port and execute platformio run in project’s directory. 

To properly run the endpoint device so that it can listen to the hub’s broadcasted messages, you have to configure the Arduino Indicator. We decided to store the configuration in two predefined addresses of the Arduino’s EEPROM memory.
In order to make the device configuration easier, we created scripts that will generate and flash the EEPROM of your indicator directly from your Raspberry Pi.

Please connect the device to the USB port and run configure_indicator.py script, where you will be asked two questions:

1. what is the device ID (every indicator in your location should have a unique ID),

2. how many Neopixel LEDs the device has.

### Contribution guidelines ###

* Feel free to publish pull requests for the project, we're open for any suggestions from you!

### Who do I talk to? ###

* Please use Bitbucket Issues on the project page.