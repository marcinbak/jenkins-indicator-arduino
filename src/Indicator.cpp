/*
 * Indicator.cpp
 *
 *  Created on: Jun 11, 2016
 *      Author: mdabrowski
 */

#include "Indicator.h"

#define OVERSAMPLE 3
static const uint64_t BROADCAST_ADDRESS = 0x65646f4e32L;

Indicator::Indicator(Adafruit_NeoPixel& aPixel, RF24& aNrf, uint8_t aDeviceCode) :
		deviceCode(aDeviceCode), currentState(UNKNOWN), pixel(aPixel), rf24(aNrf), successAnim(
				&aPixel, aPixel.numPixels()), failAnim(&aPixel,
				aPixel.numPixels()), successProgressAnim(&aPixel,
				aPixel.numPixels(),OVERSAMPLE), failProgressAnim(&aPixel,
				aPixel.numPixels(),OVERSAMPLE), animator(&aPixel) {
}

Indicator::~Indicator() {
}

void Indicator::setup() {
	Serial.print("Device code: ");
	Serial.println(deviceCode);
	Serial.print("Pixel num: ");
	Serial.println(pixel.numPixels());
	rf24.begin();
	rf24.setAutoAck(false);
	rf24.setChannel(70);
	rf24.setPALevel(RF24_PA_LOW);
	rf24.setDataRate(RF24_250KBPS);
	rf24.openReadingPipe(1, BROADCAST_ADDRESS);
	rf24.startListening();

	pixel.begin();
	for (int i = 0; i < 16; i++) {
		pixel.setPixelColor(i, 0);
		pixel.show();
	}
	pixel.setBrightness(255);

	successProgressAnim.setDuration(8000);
	successProgressAnim.setColor(Adafruit_NeoPixel::Color(0, 200, 0));

	failProgressAnim.setDuration(8000);
	failProgressAnim.setColor(Adafruit_NeoPixel::Color(200, 0, 0));


	successAnim.setDuration(6000);
	successAnim.setColor(Adafruit_NeoPixel::Color(0, 255, 0));

	failAnim.setDuration(1000);
	failAnim.setColor(Adafruit_NeoPixel::Color(255, 0, 0));

	rf24.startListening();
	animator.setAnimation(nullptr);
	Serial.println("Setup Finished");
}

void Indicator::loop() {
	uint8_t msg[3];
	uint8_t project;
	uint8_t state;
	uint8_t fill;
	bool update = false;
	if (rf24.available()) {
		rf24.read(&msg, sizeof(msg));
		project = msg[0];
		state = msg[1];
		fill = msg[2];
		Serial.print(" ProjectCode: ");
		Serial.print(project);
		Serial.print(" State: ");
		Serial.print(state);
		Serial.print(" Fill: ");
		Serial.println(fill);
		update = deviceCode == project;
		if (update) {
				Serial.print("Tail:");
				Serial.println(fill);
				update = false;
				switch (msg[1]) {
				case UNKNOWN:
					animator.setAnimation(nullptr);
					break;
				case SUCCCESS:
					animator.setAnimation(&successAnim);
					break;
				case FAIL:
					animator.setAnimation(&failAnim);
					break;
				case BUILDING_SUCCESS:
					successProgressAnim.setFill(fill);
					animator.setAnimation(&successProgressAnim);
					break;
				case BUILDING_FAIL:
					failProgressAnim.setFill(fill);
					animator.setAnimation(&failProgressAnim);
					break;
				}
			}
	}
	animator.notifyTimePass();
}
